var express = require("express");
var router = express.Router();

const util = require("util");
const mysql = require("mysql");

const connection = mysql.createConnection({
  host: "db",
  user: "root",
  password: "root",
  database: "dockerhomework",
});

/* GET home page. */
router.get("/", async function (req, res) {
  const dbLines = await getLinesFromDB();
  res.render("index", { title: "Docker compose homework", lines: dbLines });
});

module.exports = router;
const query = util.promisify(connection.query).bind(connection);

async function getLinesFromDB() {
  const result = await query("select * from testtable");
  result.forEach((val) => {
    console.log(val.teststring);
  });
  return result;
}
